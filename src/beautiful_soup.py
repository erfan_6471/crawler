from datetime import datetime

from bs4 import BeautifulSoup

html = "dongfeng_h30-crosspage_1.html"

with open(html, "r", encoding="utf-8") as f:
    y = f.read()


def extract_price(adv):
    cost = adv.find("p", "cost")
    if cost is not None:
        price_spane = cost.find(itemprop="price")
        if price_spane is not None:
            print(price_spane.get("content"))


def extract_data(adv):
    return dict(price=extract_price(adv))


data = "https://bama.ir/car"


def extract_link(li):
    a = li.find("a")
    return a.attrs.get("href", "")


def pars_html(data):
    soup = BeautifulSoup(data, features="html.parser")
    adv_div = soup.select_one(".rows")
    adv_list = adv_div.find_all("li")
    data = list()
    for li in adv_list:
        link = extract_link(li)
        # print(link)
        data.append({"link": link, "flag": False})
    return data


def pars_page(data):
    soup = BeautifulSoup(data, features="html.parser")
    data = dict(title=None, price=None, body=None, post_id=None, datetime=None)

    title_tag = soup.select_one("#titletextonly")
    if title_tag is not None:
        data["title"] = title_tag.text

    price_tag = soup.select_one(".price")
    if price_tag is not None:
        data["price"] = price_tag.text
    body_tag = soup.select_one("#postingbody")
    if body_tag is not None:
        data["body"] = body_tag.text
    post_id_tag = soup.select_one(".postinginfos > p:nth-child(1)")
    if post_id_tag is not None:
        data["post_id"] = post_id_tag.text.replace("post id: ", " ")
    date_time_tag = soup.select_one(".postinginfos > p.postinginfo.reveal > time")
    if date_time_tag is not None:
        data["date_time"] = datetime.strptime(date_time_tag.text, "%Y-%m-%d %H:%M")
    images = soup.find_all("img")
    link = set()
    for image in images:
        link.add(image.attrs["src"].replace("50x50c", "600x450"))
    return data , link
