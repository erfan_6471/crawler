import requests

from src.beautiful_soup import pars_page
from src.data import save_data, save_data_file, store_page
from src.mongo import get_links, set_flag, get_image, store_image


def crawl_site(url):
    crawl = True
    page = 0
    while crawl:
        response = requests.get(url.format(page * 120))
        page += 1
        crawl = save_data(response)
    print("####### page found is {}".format(page - 1), "#######")


def get_single_page(link):
    response = requests.get(link)
    if response.ok:
        store_page(response)
        return True
    return False


def crawl_page():
    links = get_links()
    for link in links:
        result = get_single_page(link['link'])
        if result:
            set_flag(link["_id"])


def download_one_image(link):
    respons = requests.get(link)
    if respons.ok:
        store_image(respons)
    print("done")


def download_image():
    post_images = get_image()
    for post_image in post_images:
        for image in post_image["images"]:
            download_one_image(image)


if __name__ == "__main__":
    # target = "https://manila.craigslist.org/d/apartments-housing-for-rent/search/apa?s={}"
    # crawl_site(target)
    # crawl_page()
    download_image()
