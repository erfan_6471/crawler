import random
x=random.randint(0,100000000)

from pymongo import MongoClient

client = MongoClient()

db = client.structure

links_collection = db.links

apartment_collection = db.apartment

images_collection = db.images


def save_links(data):
    return links_collection.insert_many(data)


def get_links():
    return links_collection.find({"flag": False})


def save_apartment(data):
    return apartment_collection.insert(data)


def set_flag(links_id):
    links_collection.find_one_and_update(
        {"_id": links_id}, {"$set": {"flag": True}}
    )


def save_image(data):
    return images_collection.insert(data)


def get_image():
    return images_collection.find()


def store_image(response):
    with open(f"image{x}.jpg", "wb") as f:
        f.write(response.content)
