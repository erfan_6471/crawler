from bs4 import BeautifulSoup
import requests
import pandas as pd
import json
import time

cmc = requests.get("https://coinmarketcap.com/")
soup = BeautifulSoup(cmc.content, "html.parser")

data = soup.find("script", id="__NEXT_DATA__", type="application/json")
coins = {}
coin_data = json.loads(data.contents[0])
listings = coin_data["props"]["initialState"]["cryptocurrency"]["listingLatest"]["data"]
for i in listings:
    coins[str(i["id"])] = i["slug"]

for i in coins:
    page = requests.get(f"https://coinmarketcap.com/currencies/{coins[i]}/historical-data/?start=20200101&end=20200630")
    soup = BeautifulSoup(page.content, 'html.parser')
    data = soup.find("script", id="__NEXT_DATA__", type="application/json")
    historical_data = json.loads(data.contents[0])
    quotes = historical_data['props']['initialState']['cryptocurrency']['ohlcvHistorical'][i]['quotes']
info = historical_data['props']['initialState']['cryptocurrency']['ohlcvHistorical'][i]
market_cap = []
volume = []
timestamp = []
name = []
symbol = []
slug = []
for j in quotes:
    market_cap.append(j["quote"]["USD"]["market_cap"])
    volume.append(j["quote"]["USD"]["volume"])
    timestamp.append(j["quote"]["USD"]["timestamp"])
    name.append(info["symbol"])
    slug.append(coins[i])

df = pd.DataFrame(columns=["marketcap", "volume", "timestamp", "name", "symbol", "slug"])

df["market_cap"] = market_cap
df["volume"] = volume
df["timestamp"] = timestamp
df["name"] = name
df["symbol"] = symbol

df.to_csv("criptoes.csv",index=False)
