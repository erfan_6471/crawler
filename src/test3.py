import logging

logging.basicConfig(filename="logging.log", format="[%(asctime)s]  [%(filename)s]  [%(message)s] ", level=logging.DEBUG,
                    filemode="w")
logging.debug("this msg for debug")
logging.info("this msg for info")
logging.warning("this msg for warning")
logging.critical("this msg critical")
