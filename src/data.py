from urllib.parse import urlparse

from src.beautiful_soup import pars_html, pars_page
from src.mongo import save_links, save_apartment, save_image


def get_filename(url):
    parsed = urlparse(url)
    parsed_list = parsed.path.split("/")
    filename = "_".join(parsed_list[2:4]) + parsed.query.replace("=", "_")
    filename += ".html"
    return filename


def save_data_file(response):
    with open(get_filename(response.url), "w", encoding="utf-8") as f:
        f.write(response.text)


def save_data_mongo(response):
    data = pars_html(response.text)
    if len(data):
        save_links(data)
    return len(data)


def save_data(response, db=True):
    print(response.url)
    if db:
        return save_data_mongo(response)
    return save_data_file(response)


def store_page(response):
    print(response.url)
    data, images = pars_page(response.text)
    save_apartment(data)
    save_image({"post_id": data["post_id"], "images": list(images)})
